/*
 * DialogoConexion.java
 *
 * Created on 10 de agosto de 2008, 17:48
 */
package ejemplo2b;

/**
 *
 * @author alberto
 */
import java.sql.*;

public class DialogoConexion extends javax.swing.JFrame {

  /**
   * Creates new form DialogoConexion
   */
  public DialogoConexion() {
    initComponents();
  }

  /**
   * This method is called from within the constructor to initialize
   * the form. WARNING: Do NOT modify this code. The content of this
   * method is always regenerated by the Form Editor.
   */
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    jLabel1 = new javax.swing.JLabel();
    jLabel2 = new javax.swing.JLabel();
    jTextFieldUsuario = new javax.swing.JTextField();
    jTextFieldContrasenya = new javax.swing.JTextField();
    jLabel3 = new javax.swing.JLabel();
    jTextFieldURL = new javax.swing.JTextField();
    jButtonConexion = new javax.swing.JButton();
    jScrollPane1 = new javax.swing.JScrollPane();
    jTextAreaDetalle = new javax.swing.JTextArea();

    setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
    setTitle("Conexión");
    setName("DialogoConexion"); // NOI18N
    getContentPane().setLayout(null);

    jLabel1.setText("Usuario");
    getContentPane().add(jLabel1);
    jLabel1.setBounds(10, 10, 100, 18);

    jLabel2.setText("Contraseña");
    getContentPane().add(jLabel2);
    jLabel2.setBounds(230, 10, 100, 18);

    jTextFieldUsuario.setText("paco");
    jTextFieldUsuario.setName("jTextField_Usuario"); // NOI18N
    jTextFieldUsuario.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        jTextFieldUsuarioActionPerformed(evt);
      }
    });
    getContentPane().add(jTextFieldUsuario);
    jTextFieldUsuario.setBounds(10, 50, 130, 28);
    getContentPane().add(jTextFieldContrasenya);
    jTextFieldContrasenya.setBounds(230, 50, 130, 28);

    jLabel3.setText("URL de JDBC");
    getContentPane().add(jLabel3);
    jLabel3.setBounds(130, 90, 100, 18);

    jTextFieldURL.setText("jdbc:mysql://localhost:3306/empresa");
    jTextFieldURL.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        jTextFieldURLActionPerformed(evt);
      }
    });
    getContentPane().add(jTextFieldURL);
    jTextFieldURL.setBounds(10, 110, 350, 28);

    jButtonConexion.setText("Realizar Conexión");
    jButtonConexion.setToolTipText("Formulario de conexión ");
    jButtonConexion.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        jButtonConexionActionPerformed(evt);
      }
    });
    getContentPane().add(jButtonConexion);
    jButtonConexion.setBounds(80, 150, 180, 30);

    jScrollPane1.setViewportView(jTextAreaDetalle);

    getContentPane().add(jScrollPane1);
    jScrollPane1.setBounds(10, 180, 350, 100);

    pack();
  }// </editor-fold>//GEN-END:initComponents

    private void jButtonConexionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonConexionActionPerformed
// TODO add your handling code here:
    Connection conexion = null;

    try {
      //Obtener de los jTextFields los datos
      //String usuario= dato_de_jTextFieldUsuario;
      //String pass= dato_de_jTextFieldContrasenya;
      //String jdbcUrl= dato_De_jTextFieldJDBC;

      String usuario = jTextFieldUsuario.getText();
      String pass = jTextFieldContrasenya.getText();
      String jdbcUrl = jTextFieldURL.getText();

      //Registrar el driver JDBC
      String driver = "com.mysql.jdbc.Driver";
      Class.forName(driver).newInstance();
      //registrar_driver;

      //Mostramos en jTextAreaDetalle el informe

      //*Añadir_Driver_Registrado_en_jTextAreaDetalle;
      jTextAreaDetalle.setText("Driver Registrado correctamente.\n");

      //Conectando con la Base de Datos

      //*Añadir_conectando_con_BD_en_jTextArea_Detalle;
      jTextAreaDetalle.setText(jTextAreaDetalle.getText() + "Conectando con la Base de datos...\n");

      //Obtenemos la conexión

      //*obtener_la_conexion;
      conexion = DriverManager.getConnection(jdbcUrl, usuario, pass);

      //*Añadir_conexion_establecida_en_jTextAreaDetalle;
      jTextAreaDetalle.setText(jTextAreaDetalle.getText() + "Conexion establecida con la Base de datos...\n");

    } catch (SQLException se) {
      //Errores de JDBC
      se.printStackTrace();
      //*Añadir_error_en_jTextAreaDetalle; //Método se.getMessage() --> Muestra el error
      jTextAreaDetalle.setText(se.getMessage());
    } catch (Exception e) {
      //Errores Class.forName
      e.printStackTrace();
      //*Añadir_error_en_jTextAreaDetalle; //Método e.getMessage() --> Muestra el error
      jTextAreaDetalle.setText(e.getMessage());
    } finally {
      try {
        if (conexion != null) //Cerramos conexion
        {
          conexion.close();
        }
      } catch (SQLException se) {
        se.printStackTrace();
      }//end finally try
    }//end try

    }//GEN-LAST:event_jButtonConexionActionPerformed

    private void jTextFieldUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldUsuarioActionPerformed
    // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldUsuarioActionPerformed

    private void jTextFieldURLActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldURLActionPerformed
    // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldURLActionPerformed

  /**
   * @param args the command line arguments
   */
  public static void main(String args[]) {
    java.awt.EventQueue.invokeLater(new Runnable() {
      public void run() {
        DialogoConexion d = new DialogoConexion();
        d.setSize(400, 400);
        d.setVisible(true);
      }
    });
  }
  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JButton jButtonConexion;
  private javax.swing.JLabel jLabel1;
  private javax.swing.JLabel jLabel2;
  private javax.swing.JLabel jLabel3;
  private javax.swing.JScrollPane jScrollPane1;
  private javax.swing.JTextArea jTextAreaDetalle;
  private javax.swing.JTextField jTextFieldContrasenya;
  private javax.swing.JTextField jTextFieldURL;
  private javax.swing.JTextField jTextFieldUsuario;
  // End of variables declaration//GEN-END:variables
}
