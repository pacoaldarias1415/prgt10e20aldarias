/*
 * Main.java
 * Programa para probarlo desde terminal
 */

/**
  * @author pacoaldarias
 */
import java.sql.*;

public class Main {

  public static void main(String[] args) {
	  
    Connection conexion=null;
    String driver="com.mysql.jdbc.Driver";
    String jdbcUrl="jdbc:mysql://localhost:3306/empresa";
    String usuario="paco";
    String password="";
    
    try {
      //Registrando el Driver
      Class.forName(driver).newInstance();
      //Abrir la conexion con la Base de Datos
      System.out.println("Conectando con la Base de datos...");
      conexion = DriverManager.getConnection(jdbcUrl, usuario, password);
      System.out.println("Conexion establecida con la Base de datos...");
    } catch (SQLException se) {
      //Errores de JDBC
      se.printStackTrace();
    } catch (Exception e) {
      //Errores debidos al Class.forName
      e.printStackTrace();
    } finally {
      try {
        if (conexion != null) {
          conexion.close();
        }
      } catch (SQLException se) {
        se.printStackTrace();
      }//end finally try
    }//end try
  }
}
